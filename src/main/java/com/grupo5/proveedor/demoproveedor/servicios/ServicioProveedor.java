package com.grupo5.proveedor.demoproveedor.servicios;

import com.grupo5.proveedor.demoproveedor.entidades.Proveedor;

import java.util.List;

public interface ServicioProveedor {
    List<Proveedor> listar();
    Proveedor obtenerEntidad(Long codigo);
    Proveedor nuevo (Proveedor proveedor);
    Proveedor actualizar (Proveedor proveedor);
    Proveedor eliminar  (Long codigo);

}
