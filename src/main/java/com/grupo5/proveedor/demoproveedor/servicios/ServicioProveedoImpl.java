package com.grupo5.proveedor.demoproveedor.servicios;

import com.grupo5.proveedor.demoproveedor.entidades.Proveedor;
import com.grupo5.proveedor.demoproveedor.repositorio.RepositorioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioProveedoImpl  implements  ServicioProveedor{

    @Autowired
    private RepositorioProveedor repositorioProveedor;

    @Override
    public List<Proveedor> listar() {
        return (List<Proveedor>) repositorioProveedor.findAll();
    }

    @Override
    public Proveedor obtenerEntidad(Long codigo) {
        return repositorioProveedor.findById(codigo).get();
    }

    @Override
    public Proveedor nuevo(Proveedor proveedor) {
        return repositorioProveedor.save(proveedor);
    }

    @Override
    public Proveedor actualizar(Proveedor proveedor) {
      Proveedor p = obtenerEntidad(proveedor.getCodigo());

        return repositorioProveedor.save(proveedor);
    }

    @Override
    public Proveedor eliminar(Long codigo) {
        Proveedor p = obtenerEntidad(codigo);
        repositorioProveedor.delete(p);
        return p;
    }
}
