package com.grupo5.proveedor.demoproveedor.repositorio;
import  com.grupo5.proveedor.demoproveedor.entidades.Proveedor;
import org.springframework.data.repository.CrudRepository;


public interface RepositorioProveedor extends CrudRepository<Proveedor, Long> {

}
