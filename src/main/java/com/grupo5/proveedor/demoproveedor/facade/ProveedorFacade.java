package com.grupo5.proveedor.demoproveedor.facade;

import com.grupo5.proveedor.demoproveedor.entidades.Proveedor;

import java.util.List;

public interface ProveedorFacade {

    List<Proveedor> listar();
    Proveedor obtenerEntidad(Long codigo);
    Proveedor nuevo (Proveedor proveedor);
    Proveedor actualizar (Proveedor proveedor);
    Proveedor eliminar  (Long codigo);


}



