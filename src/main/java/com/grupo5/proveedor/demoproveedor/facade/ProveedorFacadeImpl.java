package com.grupo5.proveedor.demoproveedor.facade;

import com.grupo5.proveedor.demoproveedor.entidades.Proveedor;
import com.grupo5.proveedor.demoproveedor.servicios.ServicioProveedor;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapProperties;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.xml.bind.ValidationException;
import javax.xml.crypto.Data;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")


public class ProveedorFacadeImpl implements ProveedorFacade{
    @Autowired
    private ServicioProveedor servicioProveedor;


    @GetMapping("/proveedor")
    public List<Proveedor> listar() {
        return servicioProveedor.listar();
    }

    @PostMapping("/proveedor")
    public Proveedor nuevo(@RequestBody Proveedor proveedor) {
            return servicioProveedor.nuevo(proveedor);

    }


    @PutMapping("/proveedor")
    public Proveedor actualizar(@RequestBody Proveedor proveedor) {
        Proveedor p = null;
        try{
            proveedor.getCodigo();
            p = servicioProveedor.actualizar(proveedor);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Problemas al actualizar.");
        }
        return p;
    }

    @GetMapping("/proveedor/{codigo}")
    public Proveedor obtenerEntidad(@PathVariable(value = "codigo") Long codigo) {
        return servicioProveedor.obtenerEntidad(codigo);
    }


    @DeleteMapping("/proveedor/{codigo}")
    public Proveedor eliminar(@PathVariable(value = "codigo") Long codigo) {
        return servicioProveedor.eliminar(codigo);
    }
}
