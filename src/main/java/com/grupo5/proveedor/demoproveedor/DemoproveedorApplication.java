package com.grupo5.proveedor.demoproveedor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoproveedorApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoproveedorApplication.class, args);
    }

}
